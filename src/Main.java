import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Main {
	public static void main(String args[]) throws IOException {
		int OptionNumber;
		System.out.println("******Welcome To The Jspm Voting Portal******");
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter Your Name:");
		String Name = sc.nextLine();
		System.out.println("Enter Your Age: ");
		int Age = sc.nextInt();
		System.out.print("Enter Your RollNo:");
		int RollNo = sc.nextInt();
		Student std = new Student(Name, Age, RollNo);
		String Filepath = "C:\\Users\\HP\\eclipse-workspace\\CollageVoting\\src\\ElectionRecord";
		File file = new File(Filepath);
		FileWriter fw = new FileWriter(file, true);
		BufferedWriter bw = new BufferedWriter(fw);

		bw.write(Name + " ," + Age + ", " + RollNo + " ,");
		bw.close();
		fw.close();

		OptionNumber = 0;
		while (OptionNumber != 4) {
			ShowOption();
			OptionNumber = 0;
			System.out.println("Enter Your Option: ");
			OptionNumber = sc.nextInt();
			if (OptionNumber == 1) {
				Details1();
				break;
			} else if (OptionNumber == 2) {
				Details2();
				break;
			} else if (OptionNumber == 3) {
				Details3();
				break;
			} else if (OptionNumber == 4)
			{
				Details4();
				break;
			} else{
				System.out.println(" Incorrect Option ");
			}
            

		}

	}

	public static void ShowOption() {
		System.out.println("  Candidate Name:");
		System.out.println("  1.Swapnil patil");
		System.out.println("  2.Sanket yelam");
		System.out.println("  3.sagar gonjare");
		System.out.println("  4.pratik paikekari");

	}

	public static void Details1() {
		System.out.println("your option is Swapnil Patil");
		System.out.println("your response has been Recorded");
		System.out.println("Thank You For Voting");
	}

	public static void Details2() {
		System.out.println("your option is Sanket Yelam");
		System.out.println("Your response has been Recorded");
		System.out.println("Thank you For Voting");

	}

	public static void Details3() {
		System.out.println("your option is sagar gonjare");
		System.out.println("Your response has been Recorded");
		System.out.println("Thank you For Voting");

	}

	public static void Details4() {
		System.out.println("your option is Pratik Paikekeari");
		System.out.println("Your response has been Recorded");
		System.out.println("Thank you For Voting");
	}
}
